load('@bazel_tools//tools/build_defs/repo:http.bzl', 'http_file', 'http_archive')

def external_dependencies():
    http_archive(
        name = "gcc-arm-linux-x86_64",
        urls = [
            "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2",
        ],
        sha256 = "97dbb4f019ad1650b732faffcc881689cedc14e2b7ee863d390e0a41ef16c9a3",
        strip_prefix = "gcc-arm-none-eabi-10.3-2021.10",
        build_file = "@rules_arm-none-eabi//toolchain/linux-x86_64:compiler.BUILD",
    )

    http_archive(
        name = "gcc-arm-linux-aarch64",
        urls = [
            "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-aarch64-linux.tar.bz2",
        ],
        sha256 = "f605b5f23ca898e9b8b665be208510a54a6e9fdd0fa5bfc9592002f6e7431208",
        strip_prefix = "gcc-arm-none-eabi-10.3-2021.10",
        build_file = "@rules_arm-none-eabi//toolchain/linux-aarch64:compiler.BUILD",
    )

    http_archive(
        name = "gcc-arm-windows-i686",
        urls = [
            "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-win32.zip",
        ],
        sha256 = "d287439b3090843f3f4e29c7c41f81d958a5323aecefcf705c203bfd8ae3f2e7",
        strip_prefix = "gcc-arm-none-eabi-10.3-2021.10",
        build_file = "@rules_arm-none-eabi//toolchain/windows-i686:compiler.BUILD",
    )

    http_archive(
        name = "gcc-arm-darwin-x86_64",
        urls = [
            "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-mac.tar.bz2",
        ],
        sha256 = "fb613dacb25149f140f73fe9ff6c380bb43328e6bf813473986e9127e2bc283b",
        strip_prefix = "gcc-arm-none-eabi-10.3-2021.10",
        build_file = "@rules_arm-none-eabi//toolchain/darwin-x86_64:compiler.BUILD",
    )

