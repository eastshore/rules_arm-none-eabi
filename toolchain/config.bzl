load("@bazel_tools//tools/build_defs/cc:action_names.bzl", "ACTION_NAMES")
load("@bazel_tools//tools/cpp:cc_toolchain_config_lib.bzl", "feature", "flag_group", "flag_set", "tool_path")

GCC_VERSION = "10.3.1"

_attrs = {
    "gcc": attr.string(mandatory = True),
    "ld": attr.string(mandatory = True),
    "ar": attr.string(mandatory = True),
    "cpp": attr.string(mandatory = True),
    "gcov": attr.string(mandatory = True),
    "nm": attr.string(mandatory = True),
    "objdump": attr.string(mandatory = True),
    "strip": attr.string(mandatory = True),
    "toolchain_identifier": attr.string(mandatory = True),
    "compiler_name": attr.string(mandatory = True),
    "host_system_name": attr.string(mandatory = True),
    "gcc_version": attr.string(),
}

def _flatten_list(list):
    return [ item for sublist in list for item in sublist ]

def _impl(ctx):
    tool_paths = [
        tool_path(name = "gcc", path = ctx.attr.gcc),
        tool_path(name = "ld", path = ctx.attr.ld),
        tool_path(name = "ar", path = ctx.attr.ar),
        tool_path(name = "cpp", path = ctx.attr.cpp),
        tool_path(name = "gcov", path = ctx.attr.gcov),
        tool_path(name = "nm", path = ctx.attr.nm),
        tool_path(name = "objdump", path = ctx.attr.objdump),
        tool_path(name = "strip", path = ctx.attr.strip),
    ]

    toolchain_compiler_flags = feature(
        name = "compiler_flags",
        enabled = True,
        flag_sets = [
            flag_set(
                actions = [
                    ACTION_NAMES.assemble,
                    ACTION_NAMES.preprocess_assemble,
                    ACTION_NAMES.linkstamp_compile,
                    ACTION_NAMES.c_compile,
                    ACTION_NAMES.cpp_compile,
                    ACTION_NAMES.cpp_header_parsing,
                    ACTION_NAMES.cpp_module_compile,
                    ACTION_NAMES.cpp_module_codegen,
                    ACTION_NAMES.lto_backend,
                    ACTION_NAMES.clif_match,
                ],
                flag_groups = [
                    flag_group(flags = [
                        "-isystem external/gcc-arm-{}/arm-none-eabi/include".format(
                            ctx.attr.host_system_name,
                        ),
                        "-isystem external/gcc-arm-{}/arm-none-eabi/include/c++/{}/".format(
                            ctx.attr.host_system_name,
                            GCC_VERSION,
                        ),
                        "-isystem external/gcc-arm-{}/lib/gcc/arm-none-eabi/{}/include".format(
                            ctx.attr.host_system_name,
                            GCC_VERSION,
                        ),
                    ])
                ],
            )
        ],
    )
    toolchain_linker_flags = feature(
        name = "linker_flags",
        enabled = True,
        flag_sets = [
            flag_set(
                actions = [
                    ACTION_NAMES.linkstamp_compile,
                    ACTION_NAMES.cpp_link_executable,
                    ACTION_NAMES.cpp_link_dynamic_library,
                    ACTION_NAMES.cpp_link_nodeps_dynamic_library,
                    ACTION_NAMES.cpp_link_static_library,
                ],
                flag_groups = [
                    flag_group(flags = [
                        "--specs=nosys.specs",
                        "-L external/gcc-arm-{}/arm-none-eabi/lib/".format(
                            ctx.attr.host_system_name,
                        ),
                        "-L external/gcc-arm-{}/lib/gcc/arm-none-eabi/{}/".format(
                            ctx.attr.host_system_name,
                            GCC_VERSION,
                        ),
                    ]),
                ],
            ),
        ],
    )


    return cc_common.create_cc_toolchain_config_info(
        ctx = ctx,
        toolchain_identifier = ctx.attr.toolchain_identifier,
        target_system_name = "local",
        host_system_name = ctx.attr.host_system_name,
        target_cpu = "arm-none-eabi",
        target_libc = "gcc",
        compiler = ctx.attr.compiler_name,
        abi_version = ctx.attr.gcc_version,
        tool_paths = tool_paths,
        features = [
            toolchain_compiler_flags,
            toolchain_linker_flags,
        ],
    )

cc_arm_none_eabi_config = rule(
    implementation = _impl,
    attrs = _attrs,
    provides = [CcToolchainConfigInfo],
)

