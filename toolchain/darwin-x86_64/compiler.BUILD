# toolchains/compiler.BUILD

package(default_visibility = ['//visibility:public'])

# export the executable files to make them available for direct use.
exports_files(glob(["bin/*"]))

# gcc executables.
filegroup(
    name = "gcc",
    srcs = glob(["bin/arm-none-eabi-gcc*"]),
)

# ld executables.
filegroup(
    name = "ld",
    srcs = glob(["bin/arm-none-eabi-ld*"]),
)

# nm executables.
filegroup(
    name = "nm",
    srcs = glob(["bin/arm-none-eabi-nm*"]),
)

# objcopy executables.
filegroup(
    name = "objcopy",
    srcs = glob(["bin/arm-none-eabi-objcopy*"]),
)

# objdump executables.
filegroup(
    name = "objdump",
    srcs = glob(["bin/arm-none-eabi-objdump*"]),
)

# strip executables.
filegroup(
    name = "strip",
    srcs = glob(["bin/arm-none-eabi-strip*"]),
)

# as executables.
filegroup(
    name = "as",
    srcs = glob(["bin/arm-none-eabi-as*"]),
)

# ar executables.
filegroup(
    name = "ar",
    srcs = glob(["bin/arm-none-eabi-ar*"]),
)

# size executables.
filegroup(
    name = "size",
    srcs = glob(["bin/arm-none-eabi-size*"]),
)

# libraries and headers.
filegroup(
    name = "compiler_pieces",
    srcs = glob([
        "arm-none-eabi/**",
        "lib/gcc/arm-none-eabi/**",
        "include/**",
    ]),
)

# collection of executables.
filegroup(
    name = "compiler_components",
    srcs = [
        ":ar",
        ":as",
        ":gcc",
        ":ld",
        ":nm",
        ":objcopy",
        ":objdump",
        ":strip",
    ],
)

filegroup(
    name = "arm-none-eabi-includes",
    srcs = glob([
        "arm-none-eabi/include/*",
        "arm-none-eabi/include/c++/10.3.1/*",
    ]),
)

filegroup(
    name = "linker-includes",
    srcs = glob([
        "arm-none-eabi/lib/*.a",
        "lib/gcc/arm-none-eabi/10.3.1/*.a",
    ]),
)

load("@rules_arm-none-eabi//toolchain:config.bzl", "cc_arm_none_eabi_config")

IDENTIFIER = "gcc-arm-eabi-none-darwin-x86_64"

cc_arm_none_eabi_config(
    name = "config",
    gcc = "bin/arm-none-eabi-gcc",
    ld = "bin/arm-none-eabi-ld",
    ar = "bin/arm-none-eabi-ar",
    cpp = "bin/arm-none-eabi-cpp",
    gcov = "bin/arm-none-eabi-gcov",
    nm = "bin/arm-none-eabi-nm",
    objdump = "bin/arm-none-eabi-objdump",
    strip = "bin/arm-none-eabi-strip",
    compiler_name = "gcc-arm-darwin-x86_64",
    host_system_name = "darwin-x86_64",
    toolchain_identifier = IDENTIFIER,
    visibility = [ "//visibility:public" ],
)

filegroup(
    name = "compiler_files",
    srcs = [
        ":compiler_pieces",
        ":gcc",
    ],
)

filegroup(
    name = "linker_files",
    srcs = [
        ":ar",
        ":compiler_pieces",
        ":gcc",
        ":ld",
    ]
)

filegroup(
    name = "all_files",
    srcs = [
        ":compiler_files",
        ":linker_files",
    ],
)

cc_toolchain(
    name ="cc_toolchain",
    all_files = ":all_files",
    ar_files = ":ar",
    compiler_files = ":compiler_files",
    dwp_files = ":empty",
    linker_files = ":linker_files",
    objcopy_files = ":objcopy",
    strip_files = ":strip",
    supports_param_files = 1,
    toolchain_config = ":config",
    toolchain_identifier = IDENTIFIER,
    visibility = [ "//visibility:public" ],
)
