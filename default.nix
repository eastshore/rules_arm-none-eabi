{ pkgs ? import <nixpkgs> {} }:

let
  vim-pio = pkgs.vimUtils.buildVimPlugin {
    name = "vim-pio";
    src = pkgs.fetchFromGitHub {
      owner = "normen";
      repo = "vim-pio";
      rev = "9e17deef82671c53915ef701c2ab2b12b62712f8";
      hash = "sha256-zaGxli97vzJb0ESkaiDUcJOLcBbdTuiY710iE7OBKd0=";
    };
  };

  vim-cpp-modern = pkgs.vimUtils.buildVimPlugin {
    name = "vim-cpp-modern";
    src = pkgs.fetchFromGitHub {
      owner = "bfrg";
      repo = "vim-cpp-modern";
      rev = "0f0529bf2a336a4e824a26b733220548d32697a6";
      hash = "sha256-TseU3nW891sCYcjejkuAJDXf2zBl4W6eG+IoPifbVBY=";
    };
  };

  vim-custom = pkgs.vimHugeX.customize {
    vimrcConfig.packages.myVimPackage = with pkgs.vimPlugins; {
      start = [
        vim-pio
        #vim-cpp-modern
        copilot-vim
        rainbow_parentheses
        vim-gutentags
        NeoSolarized
        vim-bazel
        python-syntax
        vim-yaml
      ];
    };

    vimrcConfig.customRC = ''
      " Preferred global default settings:
      set number                    " Enable line numbers by default
      set background=dark           " Set the default background to dark or light
      set smartindent               " Automatically insert extra level of indentation
      set tabstop=4                 " Default tabstop
      set shiftwidth=4              " Default indent spacing
      set expandtab                 " Expand [TABS] to spaces
      syntax enable                 " Enable syntax highlighting
      colorscheme NeoSolarized      " Set the default colour scheme
      set t_Co=256                  " use 265 colors in vim
      set spell spelllang=en_au     " Default spell checking language
      hi clear SpellBad             " Clear any unwanted default settings
      hi SpellBad cterm=underline   " Set the spell checking highlight style
      hi SpellBad ctermbg=NONE      " Set the spell checking highlight background
      match ErrorMsg '\s\+$'        "

      " let g:airline_powerline_fonts = 1   " Use powerline fonts
      " let g:airline_theme='NeoSolarized'  " Set the airline theme

      set laststatus=2   " Set up the status line so it's coloured and always on

      " Add more settings below

      set termguicolors

      " command CompileCPP ! g++ -I $HOME/src/ragerpager/inc %
      " noremap <C-F9> :CompileCPP<CR>

      autocmd filetype cpp :execute "command! CompileCPP ! g++ -I ./inc/ %"
      autocmd filetype cpp :execute "noremap <C-F9> :CompileCPP<CR>"
    '';
  };
in
(pkgs.buildFHSUserEnv {
  name = "bazel_rules_esp-idf";
  targetPkgs = pkgs: [
    pkgs.glibc
    pkgs.gcc
    pkgs.zlib
    pkgs.python310
    pkgs.git
    pkgs.universal-ctags
    pkgs.libusb1
    vim-custom
  ];
  # runScript = "pio";
}).env
